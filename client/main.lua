-- Constants
local INPUT_VEH_HORN = 86           -- E
local INPUT_MP_TEXT_CHAT_TEAM = 246 -- Y
local INPUT_PUSH_TO_TALK = 249      -- N

local STATE_IDLE = 0
local STATE_INVITING = 1
local STATE_INVITED = 2
local STATE_RACING = 3

-- Variables synced with server
local states = setmetatable({}, {
  __index = function() return STATE_IDLE end -- Default value
})

local function Text(x,y,text)
  SetTextEntry("STRING")
  AddTextComponentString(text)
  SetTextFont(0)
  SetTextDropShadow()
  DrawText(x,y)
end

-- Local variables
local localId = PlayerId()
local rival = nil
local notification = nil
local isLead = false

local debug = ""
Citizen.CreateThread(function()
  while true do
    local str = "Rival: "..tostring(rival).."\n"
    for _, ply in pairs(GetPlayers()) do
      str = str .. tostring(GetPlayerName(ply)) .. ": " .. tostring(states[ply]) .. "\n"
    end
    Text(.01,.01,str)
    Text(.3,.01,debug)
    Citizen.Wait(0)
  end
end)

--------------------------------------------------------------------------------
-- Find potential rival
--------------------------------------------------------------------------------

Citizen.CreateThread(function ()
  local function IsRivalAvailable(rivalId)
    if states[rivalId] ~= STATE_IDLE then
      return false
    end

    local localPed = PlayerPedId()
    local rivalPed = GetPlayerPed(rivalId)

    return CanPedRacePed(localPed, rivalPed)
  end

  local function FindNearestAvailableRival()
    for _, rivalId in ipairs(GetPlayers()) do
      if IsRivalAvailable(rivalId) then
        return rivalId
      end
    end

    return nil
  end

  local function CheckForNearbyRivals()
    local isDriving = IsPedDriving(PlayerPedId())

    if not isDriving or states[localId] ~= STATE_IDLE then
      return
    end

    local nearbyRival = FindNearestAvailableRival()

    if rival == nearbyRival then
      return
    end

    if notification then
      RemoveNotification(notification)
    end

    if nearbyRival then
      local name = GetPlayerName(nearbyRival)
      local text = '~w~Press ~o~E ~w~to race ' .. name .. '!'

      notification = CreateNotification(text)
    end

    rival = nearbyRival
  end

  while true do
    Citizen.Wait(500)
    CheckForNearbyRivals()
  end
end)

--------------------------------------------------------------------------------
-- Keys!
--------------------------------------------------------------------------------

Citizen.CreateThread(function ()
  local function IsDoubleHonk()
    if not IsControlJustReleased(0, INPUT_VEH_HORN) then return 0 end
    firstPress = GetGameTimer()
    Citizen.Wait(0)

    while not IsControlJustReleased(0, INPUT_VEH_HORN) do
      debug = "First: "..firstPress.."\n"
            .."Curr:  "..GetGameTimer().."\n"
            .."Diff:  "..(GetGameTimer() - firstPress)
      if GetGameTimer() - firstPress > 500 then
        return 1 -- Took more than .5s to press again
      end
      Citizen.Wait(0)
    end

    return 2
  end


  local function SendInvite()
    if notification then
      RemoveNotification(notification)
    end

    CreateNotification('Invite sent!\n~o~Honk ~w~twice to ~r~cancel~w~')
    TriggerServerEvent('outrun:__sendInvite', GetPlayerServerId(rival))
  end

  local function AcceptInvite()
    if states[localId] ~= STATE_INVITED then return end

    if notification then
      RemoveNotification(notification)
    end

    CreateNotification('Invite accepted.')
    TriggerServerEvent('outrun:__acceptInvite')
  end

  local function CancelInvite()
      if notification then
        RemoveNotification(notification)
      end

      CreateNotification('Invite declined.')
      TriggerServerEvent('outrun:__cancelInvite')
  end

  while true do
    if states[localId] == STATE_IDLE then
      -- Use IsControlPressed instead of _Just_ so we can start pressing before message
      if IsControlPressed(0, INPUT_VEH_HORN) and rival then
        SendInvite()
        while IsControlPressed(0, INPUT_VEH_HORN) do
          Citizen.Wait(0)
        end
      end
    -- Not IDLE and not RACING, so INVITING or INVITED
    elseif states[localId] ~= STATE_RACING then
      local honks = IsDoubleHonk()
      if honks == 1 and states[localId] == STATE_INVITED then
          AcceptInvite()
      elseif honks == 2 then
          CancelInvite()
      end
    end
    Citizen.Wait(0)
  end
end)

--[[Citizen.CreateThread(function ()
  local function CheckForRaceInviteRequest()
    if states[localId] ~= STATE_IDLE or not rival then
      return
    end

    if IsControlJustPressed(0, INPUT_VEH_HORN) then
      if notification then
        RemoveNotification(notification)
      end

      CreateNotification('Invite sent.')
      TriggerServerEvent('outrun:__sendInvite', GetPlayerServerId(rival))
      return
    end
  end

  while true do
    Citizen.Wait(0)
    CheckForRaceInviteRequest()
  end
end)

--------------------------------------------------------------------------------

Citizen.CreateThread(function ()
  local function CheckForRaceInviteResponse()
    if states[localId] ~= STATE_INVITED then
      return
    end

    if IsControlJustPressed(0, INPUT_MP_TEXT_CHAT_TEAM) then
      if notification then
        RemoveNotification(notification)
      end

      CreateNotification('Invite accepted.')
      TriggerServerEvent('outrun:__acceptInvite')

      return
    end

    if IsControlJustPressed(0, INPUT_PUSH_TO_TALK) then
      if notification then
        RemoveNotification(notification)
      end

      CreateNotification('Invite declined.')
      TriggerServerEvent('outrun:__cancelInvite')

      return
    end
  end

  while true do
    Citizen.Wait(0)
    CheckForRaceInviteResponse()
  end
end)]]

--------------------------------------------------------------------------------

Citizen.CreateThread(function ()
  local function CheckForOvertakes()
    if states[localId] ~= STATE_RACING or not isLead then
      return
    end

     -- Do something here.
  end

  while true do
    Citizen.Wait(0)
    CheckForOvertakes()
  end
end)

--------------------------------------------------------------------------------

Citizen.CreateThread(function ()
  local function CheckForRunDistance()
    if states[localId] ~= STATE_RACING or not isLead then
      return
    end

    -- Do something here.
    -- GenerateDirectionsToCoord
  end

  while true do
    Citizen.Wait(0)
    CheckForRunDistance()
  end
end)

--------------------------------------------------------------------------------

AddEventHandler('onClientResourceStart', function ()
  TriggerServerEvent('outrun:__sync')
end)

--------------------------------------------------------------------------------

RegisterNetEvent('outrun:__init')
AddEventHandler('outrun:__init', function (serverStates)
  states = setmetatable({},{
    __index = function() return STATE_IDLE end -- Default value
  })
  for servId, state in pairs(serverStates) do
    rivalId = GetPlayerFromServerId(servId)
    states[rivalId] = state
  end
end)

--------------------------------------------------------------------------------

RegisterNetEvent('outrun:onClientRaceInviteRequest')
AddEventHandler('outrun:onClientRaceInviteRequest', function (inviter, invitee)
  local inviter = GetPlayerFromServerId(inviter)
  local invitee = GetPlayerFromServerId(invitee)

  debug = "onClientRaceInviteRequest\n"..
          "Inviter: "..inviter.."\nInvitee: "..invitee.."\n"

  states[inviter] = STATE_INVITING

  if invitee ~= localId then
    states[invitee] = STATE_INVITED
    return
  end

  if states[localId] ~= STATE_IDLE then
    -- This is a really bad scenario, just abort
    TriggerServerEvent('outrun:__cancelInvite')
    return
  end

  states[localId] = STATE_INVITED

  if notification then
    RemoveNotification(notification)
  end

  local name = GetPlayerName(challenger)
  local text = '~b~' .. name .. ' ~w~wants to race you!\n'
            .. '~o~Honk ~w~once to ~g~accept ~w~or twice to ~r~decline~w~'

  notification = CreateNotification(text)
end)

--------------------------------------------------------------------------------

RegisterNetEvent('outrun:onClientRaceInviteAccept')
AddEventHandler('outrun:onClientRaceInviteAccept', function (inviter, invitee)
  inviter = GetPlayerFromServerId(inviter)
  invitee = GetPlayerFromServerId(invitee)

  debug = "onClientRaceInviteAccept\n"..
          "Inviter: "..inviter.."\nInvitee: "..invitee.."\n"

  if inviter ~= localId then return end

  if notification then
    RemoveNotification(notification)
  end

  local name = GetPlayerName(invitee)
  local text = '~w~' .. name .. ' accepted your invite!'

  notification = CreateNotification(text)

end)

--------------------------------------------------------------------------------

RegisterNetEvent('outrun:onClientRaceInviteDecline')
AddEventHandler('outrun:onClientRaceInviteDecline', function (inviter, invitee)
  inviter = GetPlayerFromServerId(inviter)
  invitee = GetPlayerFromServerId(invitee)

  debug = "onClientRaceInviteDecline\n"..
          "Inviter: "..inviter.."\nInvitee: "..invitee.."\n"

  states[inviter] = STATE_IDLE
  states[invitee] = STATE_IDLE

  if inviter ~= localId then return end

  if notification then
    RemoveNotification(notification)
  end

  local name = GetPlayerName(invitee)
  local text = '~w~' .. name .. ' declined your invite.'

  notification = CreateNotification(text)
end)

--------------------------------------------------------------------------------

RegisterNetEvent('outrun:onClientRaceStart')
AddEventHandler('outrun:onClientRaceStart', function (challenger, opponent)
  challenger = GetPlayerFromServerId(challenger)
  opponent = GetPlayerFromServerId(opponent)

  debug = "onClientRaceStart\n"..
          "Challenger: "..challenger.."\nOpponent: "..opponent.."\n"

  states[challenger] = STATE_RACING
  states[opponent] = STATE_RACING

  if challenger ~= localId and opponent ~= localId then
    return
  end

  if notification then
    RemoveNotification(notification)
  end

  local text = 'Race started, takes about 5 seconds!'

  notification = CreateNotification(text)

  -- Math random to make it a "race"
  SetTimeout(math.random(4500,5500), function ()
    TriggerServerEvent('outrun:__end')
  end)
end)

--------------------------------------------------------------------------------

RegisterNetEvent('outrun:onClientRaceEnd')
AddEventHandler('outrun:onClientRaceEnd', function (winner, loser)
  winner = GetPlayerFromServerId(winner)
  loser = GetPlayerFromServerId(loser)

  debug = "onClientRaceEnd\n"..
          "Winner: "..winner.."\nLoser: "..loser.."\n"

  states[winner] = STATE_IDLE
  states[loser] = STATE_IDLE

  if winner ~= localId and loser ~= localId then
    return
  end

  if notification then
    RemoveNotification(notification)
  end

  local text = 'Race ended!\n'
            .. '~b~' .. GetPlayerName(winner) .. ' ~w~won'

  notification = CreateNotification(text)
end)
