function GetPlayers()
  local players = {}

  for playerId = 0, 32 do
    if NetworkIsPlayerActive(playerId) then
      table.insert(players, playerId)
    end
  end

  return players
end

function IsPedDriving(ped)
  if not IsPedInAnyVehicle(ped) then
    return false
  end

  local vehicle = GetVehiclePedIsIn(ped)
  return GetPedInVehicleSeat(vehicle, -1) == ped
end

function CanPedRacePed(localPed, rivalPed)
  if localPed == rivalPed then
    return false
  end

  local localVehicle = GetVehiclePedIsIn(localPed)
  local rivalVehicle = GetVehiclePedIsIn(rivalPed)

  if localVehicle == 0 or rivalVehicle == 0 then
    return false
  end

  local localIsDriver = GetPedInVehicleSeat(localVehicle, -1) == localPed
  local rivalIsDriver = GetPedInVehicleSeat(rivalVehicle, -1) == rivalPed

  if not localIsDriver or not rivalIsDriver then
    return false
  end

  local localPos = GetEntityCoords(localVehicle)
  local rivalPos = GetEntityCoords(rivalVehicle)
  local distance = #(localPos - rivalPos)

  if distance > 15 then
    return false
  end

  local localSpeed = GetEntitySpeed(localVehicle)
  local rivalSpeed = GetEntitySpeed(rivalVehicle)
  local speedDiff = math.abs(localSpeed - rivalSpeed)

  if speedDiff > 15 then
    return false
  end

  local localHeading = GetEntityHeading(localVehicle)
  local rivalHeading = GetEntityHeading(rivalVehicle)
  local headingDiff = math.abs(localHeading - rivalHeading)

  if headingDiff > 65 then
    return false
  end

  return true
end

function CreateNotification(text)
  SetNotificationTextEntry('STRING')
  AddTextComponentString(text)

  return DrawNotification(false, true)
end
