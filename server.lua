-- Constants
local STATE_IDLE = 0
local STATE_INVITING = 1
local STATE_INVITED = 2
local STATE_RACING = 3


local states = setmetatable({},{
  __index = function() return STATE_IDLE end -- Default value
})
local invites = {} -- [invitee] = inviter
local races = {} -- [a] = b, [b] = a It's 2 way :O

RegisterNetEvent('outrun:__sync')
AddEventHandler('outrun:__sync', function ()
  -- NOTE: Temporary runtime fix.
  local source = source

  TriggerClientEvent('outrun:__init', source, states)
end)

AddEventHandler('outrun:__start', function (challenger, opponent)
  states[challenger] = STATE_RACING
  states[opponent] = STATE_RACING

  races[challenger] = opponent
  races[opponent] = challenger

  TriggerEvent('outrun:onRaceStart', challenger, opponent)
  TriggerClientEvent('outrun:onClientRaceStart', -1, challenger, opponent)
end)

RegisterNetEvent('outrun:__end')
AddEventHandler('outrun:__end', function ()
  -- NOTE: Temporary runtime fix.
  local winner = source
  local loser = races[winner]

  races[winner] = nil
  races[loser] = nil

  states[winner] = STATE_IDLE
  states[loser] = STATE_IDLE

  TriggerEvent('outrun:onRaceEnd', winner, loser)
  TriggerClientEvent('outrun:onClientRaceEnd', -1, winner, loser)
end)

RegisterNetEvent('outrun:__sendInvite')
AddEventHandler('outrun:__sendInvite', function (invitee)
  -- NOTE: Temporary runtime fix.
  local inviter = source
  print("__sendInvite\n"..
          "Inviter: "..inviter.."\nInvitee: "..invitee.."\n")

  if states[inviter] ~= STATE_IDLE then return end
  if states[invitee] ~= STATE_IDLE then return end

  states[inviter] = STATE_INVITING
  states[invitee] = STATE_INVITED

  invites[invitee] = inviter

  TriggerEvent('outrun:onRaceInviteRequest', inviter, invitee)
  TriggerClientEvent('outrun:onClientRaceInviteRequest', -1, inviter, invitee)
end)

RegisterNetEvent('outrun:__acceptInvite')
AddEventHandler('outrun:__acceptInvite', function ()
  -- NOTE: Temporary runtime fix.
  local invitee = source

  if not invites[invitee] then return end

  local inviter = invites[invitee]
  invites[invitee] = nil

  -- This is kinda awkward as there's no intermediary state

  print("__acceptInv ite\n"..
           "Inviter: "..inviter.."\nInvitee: "..invitee.."\n")

  TriggerEvent('outrun:onRaceInviteAccept', inviter, invitee)
  TriggerClientEvent('outrun:onClientRaceInviteAccept', -1, inviter, invitee)

  TriggerEvent('outrun:__start', inviter, invitee)
end)

RegisterNetEvent('outrun:__cancelInvite')
AddEventHandler('outrun:__cancelInvite', function ()
  -- NOTE: Temporary runtime fix.
  local source = source
  local inviter, invitee

  if invites[source] then
    invitee = source
    inviter = invites[source]
  else
    -- This way the inviter can cancel it as well
    for jnviter, jnvitee in pairs(invites) do
      if jnvitee == source then
        inviter = jnviter
        invitee = jnvitee
      end
    end
  end

  if not inviter or not invitee then return end

  invites[invitee] = nil

  states[invitee] = STATE_IDLE
  states[inviter] = STATE_IDLE

  TriggerEvent('outrun:onRaceInviteDecline', inviter, invitee)
  TriggerClientEvent('outrun:onClientRaceInviteDecline', -1, inviter, invitee)
end)
